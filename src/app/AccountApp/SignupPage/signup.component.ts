import { Component } from '@angular/core';
import { Login, Registration, CheckMobileNumber } from '../Models/account';
import { SignupService } from '../Services/signup.service';
import { Router } from "@angular/router";
import { SharedService } from '../../UtilityApp/Services/shared.service';
import { NotificationService } from '../../UtilityApp/Services/notification.service';
import { LocalstorageService } from '../../UtilityApp/Services/localstorage.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html'
})

export class SignupComponent {

  countries: any[];
  countryData: any;
  registerationDtls: any;
  subDomainName: string = "";
  str1: string = "";
  otpCode: number;
  i: number;
  logindata : any[];
  mobNumber:string = "";
  length: number;
  passwd: string = "";
  flag: number = 0;
  registration: Registration = new Registration();

  constructor(private signupService: SignupService, private router: Router, private sharedService: SharedService,
    private notificationService: NotificationService, private localstorageService: LocalstorageService) {
    localStorage.removeItem('userData');

   if (this.sharedService.getDataByKey("countryCode") != undefined && this.sharedService.getDataByKey("countryCode") != null) {
      this.registration.countryCode = JSON.parse(this.sharedService.getDataByKey("countryCode"));
    }


    if (this.sharedService.getDataByKey("mobileNumber") != undefined && this.sharedService.getDataByKey("mobileNumber") != null) {
      this.registration.phone = "+" + JSON.parse(this.sharedService.getDataByKey("countryCode")) + " " +JSON.parse(this.sharedService.getDataByKey("mobileNumber"));
    }

    this.registration.password = "12345";

    this.subDomainName = "modern";

    this.countries = this.sharedService.getCountries();

    this.countryData = this.countries[0].Id;

    localstorageService.setData("country", this.countries[0]);
    
    this.str1 = this.registration.domainName;
   // this.registration.password = JSON.parse(this.sharedService.getDataByKey("password"));
   // this.registration.domainName = JSON.parse(this.sharedService.getDataByKey("domainName"));

  }

  registerUser(registration: Registration) {

    registration.domainName = this.subDomainName; // + '.dev.test.com';
   
    this.registerationDtls = {"password": this.registration.password, "domainName": this.registration.domainName, "mobileNumber": this.registration.phone, "countryCode": this.registration.countryCode};
    let obj :any;
   // obj =this.localstorageService.getData("UserModel");

  //  if(registration.phone == "7798554631" && registration.password == "12345" && registration.domainName == "modern"){
  //       this.flag = 1;
  //  }

    // if(obj){
       
    //      if(obj.length == 1){
    //         obj.push(this.registerationDtls);
    //         this.flag = 1;
    //      }
         
    //   }
    // else
    //   {
    //   obj = [];
    //   this.length = obj.length;

    //   for(let i = 0; i< this.length; i++){
    //     if(this.registerationDtls[i].mobileNumber != registration.phone){
    //      obj.push(this.registerationDtls);
    //      this.flag = 1;
    //   }
    //   }
    //   }
     this.localstorageService.setData("UserModel",this.registerationDtls);
    
   // if(this.flag == 1){
    this.sharedService.setData("mobileNumber", registration.phone);
    this.sharedService.setData("password", registration.password);
    this.sharedService.setData("domainName", registration.domainName);
    this.sharedService.setData("countrycode", registration.countryCode);
    this.router.navigate(['confirmMobile']);
   // }
  //else{
   //  this.notificationService.success("Login Unsuccessfull", "Mobile number already exists.");
 // }
    

    // this.logindata = this.localstorageService.getData("UserModel");

    // this.length = this.logindata.length;

    // for(let i = 0; i< this.length; i++){
    //   this.mobNumber = this.logindata[i].mobileNumber; 
    //   this.passwd = this.logindata[i].password;
    //   this.length = this.logindata.length;
    //   if(this.mobNumber == registration.phone && this.passwd == registration.password){
    //   this.flag++;
    //   if(this.flag == 1){
    //       this.sharedService.setData("mobileNumber", registration.phone);
    //       this.sharedService.setData("password", registration.password);
    //       this.sharedService.setData("domainName", registration.domainName);
    //       this.sharedService.setData("countrycode", registration.countryCode);
    //       this.router.navigate(['confirmMobile']);  
    //   }  
    //   else{
    //      this.notificationService.success("Login Unsuccessfull", "Invalid mobile number or password.");
    //   } 
    //   }
    // }
     
    // if(this.flag == 0)
    //   {
    // this.sharedService.setData("mobileNumber", registration.phone);
    // this.sharedService.setData("password", registration.password);
    // this.sharedService.setData("domainName", registration.domainName);
    // this.sharedService.setData("countrycode", registration.countryCode);
    // this.router.navigate(['confirmMobile']);  
    // }
    

    // this.signupService.registerUser(registration)
    //   .then(result => {
    //     if (result.status == 200) {
    //       this.sharedService.setData("mobileNumber", registration.phone);
    //       this.sharedService.setData("countryCode", registration.countryCode);
    //       this.notificationService.success("Signup", this.signupService.checkStatusCode(result.status));
    //       this.router.navigate(['confirmMobile']);
    //     }
    //     else {
    //       this.notificationService.success("Signup", this.signupService.checkStatusCode(result.status));
    //     }
    //   });

  }

  //  changeCountry(id: number) {
  //   this.localstorageService.setData("country", this.countries[id]);
  // }
}