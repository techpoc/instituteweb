import { Component } from '@angular/core';
import { SharedService } from '../../../UtilityApp/Services/shared.service';

@Component({
  selector: 'hosteladditionaldetail',
  templateUrl: './hosteladditionaldetail.component.html'
})
export class HostelAdditionDetailComponent {
  //title = 'app works!';
  constructor(private shared:SharedService){
          this.shared.emitTypeBroadcast();
        }
}
