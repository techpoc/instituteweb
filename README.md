# INSTALLATION STEPS TO RUN THE PROJECT LOCALLY

# Institutewebapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

# Installation Setup Pre-Requisites

Both the CLI and generated project have dependencies that require

1.Node 6.9.0 or higher, together with NPM 3 or higher.

2.Source Tree latest version

# Installation Procedure

npm install -g @angular/cli


## Generating and serving an Angular project via Bitbucket && Source Tree

# Cloning a project through BitBucket

  1.open Source tree click on clone the project
  
  2.open bitbucket
  
  3.Go to Edumy-->instituteweb-->Branches
  
  4.Click on 'develop-poc' branch and take out the copy of checkout link
  
  5.paste the checkout link in bitbucket to clone
  
  6.Give a directory name there for local directory
  
  7.click on clone
  
# Executing the project  
  
  1.Go to terminal window
  
  2.Redirect to the project folder 'cd develop-poc'
  
  3.npm install -g @angular/cli@latest
  
  4.If needed run 'npm install'
  
  5.ng serve/npm start 
 
  
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Commit & Push the changes through Source tree

  1.Open source tree
  
  2.click on 'commit' to see the changes
  
  3.click 'stage all' 
  
  4.Enter a message and click on commit
  
  5.click on 'push' to move all the changes

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
